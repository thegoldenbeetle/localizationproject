#!/usr/bin/env python3

import rospy
import message_filters

from pathlib import Path
from argparse import Namespace

from sensor_msgs.msg import Imu, NavSatFix
import geometry_msgs
import tf
import tf2_ros
from tf2_msgs.msg import TFMessage
from pyproj import Proj, Transformer
from geometry_msgs.msg import TransformStamped, TwistStamped, Transform
import numpy as np
import geo_utils


def get_tf_message(timestamp, frame_id, child_frame_id, tf_matrix):
    tf_oxts_msg = TFMessage()
    tf_oxts_transform = TransformStamped()
    tf_oxts_transform = geometry_msgs.msg.TransformStamped()
    tf_oxts_transform.header.stamp = timestamp
    tf_oxts_transform.header.frame_id = frame_id
    tf_oxts_transform.child_frame_id = child_frame_id

    transform = tf_matrix
    t = transform[0:3, 3]
    q = tf.transformations.quaternion_from_matrix(transform)
    oxts_tf = Transform()

    oxts_tf.translation.x = t[0]
    oxts_tf.translation.y = t[1]
    oxts_tf.translation.z = t[2]

    oxts_tf.rotation.x = q[0]
    oxts_tf.rotation.y = q[1]
    oxts_tf.rotation.z = q[2]
    oxts_tf.rotation.w = q[3]

    tf_oxts_transform.transform = oxts_tf
    tf_oxts_msg.transforms.append(tf_oxts_transform) 

    return tf_oxts_transform 


class LocalizationNode:

    def __init__(self):

        rospy.init_node('gps_imu_localization')

        # ROS stuff
        self.gps = message_filters.Subscriber('/kitti/oxts/gps/fix', NavSatFix)
        self.imu = message_filters.Subscriber('/kitti/oxts/imu', Imu)

        self.ts = message_filters.TimeSynchronizer([self.gps, self.imu], 10)
        self.ts.registerCallback(self.callback)

        self.br = tf2_ros.TransformBroadcaster()

        #self.mercator_transformer = Transformer.from_crs("EPSG:4326", "EPSG:3857")
        self.origin = None
        self.scale = None
 

    def callback(self, gps: NavSatFix, imu: Imu):
        
        params = {'lat': gps.latitude, 'lon': gps.longitude, 'alt': gps.altitude}
        orientation = (imu.orientation.x, imu.orientation.y, imu.orientation.z, imu.orientation.w)
        euler = tf.transformations.euler_from_quaternion(orientation)
        params['roll'], params['pitch'], params['yaw'] = euler

        if self.scale is None:
            self.scale = np.cos(gps.latitude * np.pi / 180.)

        R, t = geo_utils.pose_from_oxts_packet(params, self.scale)

        if self.origin is None:
            self.origin = t

        tf_matrix = geo_utils.transform_from_rot_trans(R, t - self.origin)
        tf_msg = get_tf_message(gps.header.stamp, 'world', 'base_link', tf_matrix)

        self.br.sendTransform(tf_msg)


if __name__ == "__main__":
    node = LocalizationNode()
    rospy.spin()
